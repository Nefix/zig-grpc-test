const std = @import("std");
const c = @cImport({
    @cInclude("helloworld.grpc.pb.h");
});


pub fn main() anyerror!void {
    const svc = c.Greeter{};
    std.log.info("All your codebase are belong to us.", .{});
}
